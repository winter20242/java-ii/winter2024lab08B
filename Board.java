import java.util.Random;


public class Board {
    private Tile[][] grid;

    public Board() {
        Random rng = new Random();
        grid = new Tile[5][5];
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                grid[i][j] = Tile.BLANK;
            }
        }
        for (int i = 0; i < grid.length; i++) {
            grid[i][rng.nextInt(grid[i].length)] = Tile.HIDDEN_WALL;
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                sb.append(grid[i][j].getName()).append(" ");
            }
            sb.append("\n");
        }
        return sb.toString();
    }

    public int placeToken(int row, int col) {
        if (row < 0 || row >= grid.length || col < 0 || col >= grid.length) {
            return -2;
        }
        if (grid[row][col] == Tile.CASTLE || grid[row][col] == Tile.WALL) {
            return -1;
        }
        if (grid[row][col] == Tile.HIDDEN_WALL) {
            grid[row][col] = Tile.WALL;
            return 1;
        }

        if (grid[row][col] == Tile.BLANK) {
            grid[row][col] = Tile.CASTLE;
            return 0;
        }

        return 0;
    }

}