import java.util.Scanner;

public class BoardGameApp {
    public static void main(String[] args) {
        System.out.println("Hello dear user, welcome to \033[3mSimple Board Game\033[0m");
        Board board = new Board();

        int numCastles = 5;
        int turns = 8;

        Scanner scanner = new Scanner(System.in);

        while (numCastles > 0 && turns > 0) {
            System.out.println(board);
            System.out.println("Castles = " + numCastles);
            System.out.println("Turns = " + turns);

            System.out.print("User, enter row and column (0-5): ");

            int row = scanner.nextInt();
            int col = scanner.nextInt();

            int boardResult = board.placeToken(row, col);
            
            System.out.println(board);
            System.out.println();

            if (boardResult < 0)
                System.out.println("Re-enter a new value for row and column (0 - 5)");

            if (boardResult == 1) {
                System.out.println("There was a wall there buddy !");
                turns--;
            }
            
            if (boardResult == 0) {
                System.out.println("A wall was successfully placed !");
                turns--;
                numCastles--;
            }

        }
        scanner.close();

        if (numCastles == 0) {
            System.out.println("You won, congrats !");
        } else
            System.out.println("You lost, it's easy game, unbelievable");
    }
}